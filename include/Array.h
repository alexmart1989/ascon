#pragma once

#include "Point.h"

#include <vector>
#include <iostream>

namespace coordinate {

class Array: public std::vector<Point>
{
public:
  Array() = default;
  Array(std::string const & path);
  bool operator==(Array const & rhs) const;
  bool operator!=(Array const & rhs) const;
public:
  bool isSubset(Array const & obj) const;
private:
  bool equal(Array const & rhs) const;
  void deleteDuplicates(Array& array) const;
};

inline std::ostream& operator<<(std::ostream& out, Array const & array) {
  for(auto const & point : array) {
    out << point << " ";
  }
  return out;
}

} //namespace coordinate

