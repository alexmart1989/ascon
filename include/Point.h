#pragma once

#include <utility>
#include <iostream>

namespace coordinate {

using Coordinate = std::pair<double, double>;

class Point
{
public:
  Point() = default;
  Point(Coordinate const & point) : m_point(point) {}
  Point(Point const & obj) : m_point(obj.m_point)  {}
  Point& operator=(Point const & obj);

  bool operator==(Point const & obj) const;
  bool operator!=(Point const & obj) const;
  bool operator<(Point  const & rhs) const;
public:
  Coordinate const & getPoint() const { return m_point;        }
  double             getX()     const { return m_point.first;  }
  double             getY()     const { return m_point.second; }
private:
  Coordinate m_point;
};

inline std::ostream &operator<<(std::ostream &out, Point const &point) {
  out << "(" << point.getX() << "; " << point.getY() << ")";
}

} //namespace coordinate


