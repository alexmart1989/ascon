#include "Point.h"
#include "Array.h"

#include <iostream>

void compare(coordinate::Array const & first,
             coordinate::Array const & second)
{
  std::cout << " comparing to arrays..." << std::endl;
  std::cout << first  << std::endl;
  std::cout << second << std::endl;
  if(first != second) {
    std::cout << " is NOT equal " << std::endl;
  } else {
    std::cout << " is equal "     << std::endl;
  }

  if(first.isSubset(second)) {
    std::cout << first << " is subset of " << second << std::endl;
  } else {
    std::cout << first << " is NOT subset of " << second << std::endl;
  }
}

int main(/*int argc, char** argv*/)
{
  coordinate::Array first ("1.txt");
  coordinate::Array second("2.txt");
  coordinate::Array third ("3.txt");
  coordinate::Array fourth("4.txt");

  compare(first, second);
  compare(second, first);
  compare(first, third);
  compare(first, fourth);
  compare(second, third);
  compare(third, second);
  compare(second, fourth);
  compare(third, fourth);
  compare(fourth, third);

  return 0;
}
