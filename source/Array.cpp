#include "Array.h"

#include <algorithm>
#include <iostream>
#include <sstream>
#include <fstream>
#include <string>

namespace coordinate {

Array::Array(std::string const &path) {
  std::ifstream infile(path, std::ifstream::in);
  if(!infile.is_open()) {
    std::cout << "file not open!";
    return;
  }

  std::string line;
  while(getline(infile, line))
  {
    std::stringstream ss(line);
    double x = 0;
    double y = 0;
    ss >> x >> y;
    this->push_back(std::make_pair(x, y));
  }
}

bool Array::operator==(Array const &rhs) const {
  if(this->size() == rhs.size()) {
    return equal(rhs);
  } else {
    Array current = *this;
    deleteDuplicates(current);
    Array compared = rhs;
    deleteDuplicates(compared);
    return std::equal(current.begin(), current.end(), compared.begin());
  }
}

bool Array::operator!=(Array const &rhs) const {
  return !operator ==(rhs);
}

bool Array::isSubset(Array const &obj) const {
  Array subset = *this;
  Array set = obj;
  std::sort(subset.begin(), subset.end());
  std::sort(set.begin(),    set.end());
  return std::includes(set.begin(), set.end(), subset.begin(), subset.end());
}

bool Array::equal(Array const &rhs) const {
  return std::equal(this->begin(), this->end(), rhs.begin());
}

void Array::deleteDuplicates(Array &array) const {
  std::sort(array.begin(), array.end());
  array.erase(std::unique(array.begin(), array.end()), array.end());
}

} //namespace coordinate
