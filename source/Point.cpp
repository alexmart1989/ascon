#include "Point.h"

#include <cmath>

#define PRECISION 0.01

namespace coordinate {

Point &Point::operator=(Point const &obj) {
  m_point = obj.getPoint();
  return *this;
}

bool Point::operator==(Point const &obj) const {
  return std::abs(m_point.first - obj.m_point.first)   < PRECISION
      && std::abs(m_point.second - obj.m_point.second) < PRECISION;
}

bool Point::operator!=(Point const &obj) const {
  return !operator ==(obj);
}

bool Point::operator<(Point const &rhs) const {
  return (rhs.m_point.first - m_point.first)          > PRECISION
      || std::abs(m_point.first - rhs.m_point.first) < PRECISION
      && (rhs.m_point.second - m_point.second)       > PRECISION;
}

} //namespace coordinate

